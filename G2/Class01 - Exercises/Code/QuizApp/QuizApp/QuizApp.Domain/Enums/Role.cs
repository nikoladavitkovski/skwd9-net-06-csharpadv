﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApp.Domain.Enums
{
    public enum Role
    {
        Teacher = 1,
        Student
    }
}
