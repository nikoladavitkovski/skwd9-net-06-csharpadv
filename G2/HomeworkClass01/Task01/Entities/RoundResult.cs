﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task01.Entities
{
    public class RoundResult
    {
        public int Winner { get; set; }
        public string Message { get; set; }
    }
}
