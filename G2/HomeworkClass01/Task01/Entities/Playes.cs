﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task01.Entities
{
    public class Player
    {
        public string Name { get; set; }
        public int Won { get; set; }
        public int Lost { get; set; }
        public int Draw { get; set; }
    }
}
